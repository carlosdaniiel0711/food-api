using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using FoodAPI.Services;
using FoodAPI.Models;
using FoodAPI.Models.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace FoodAPI.Controllers
{
    [ApiController]
    [Route("api/v1/restaurants")]
    public class RestaurantsController : ControllerBase
    {
        private readonly IRestaurantService _service;
        private readonly IMenuService _menuService;
        private readonly IReviewService _reviewService;

        public RestaurantsController(IRestaurantService service, IMenuService menuService, IReviewService reviewService)
        {
            _service = service;
            _menuService = menuService;
            _reviewService = reviewService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Restaurant>>> GetAll() 
        {
            return Ok(await _service.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Restaurant>> GetById(string id) 
        {
            return Ok(await _service.GetById(id));
        }

        [HttpGet("{id}/menu")]
        public async Task<ActionResult<IEnumerable<Menu>>> GetMenu(string id)
        {
            return Ok(await _menuService.GetByRestaurantId(id));
        }

        [HttpGet("{id}/reviews")]
        public async Task<ActionResult<IEnumerable<Review>>> GetReviews(string id)
        {
            return Ok(await _reviewService.GetByRestaurantId(id));
        }

        [Authorize]
        [HttpPost("{id}/reviews")]
        public async Task<ActionResult<IEnumerable<Review>>> InsertReview(string id, [FromBody] AddReviewDTO reviewDTO)
        {
            return Ok(await _reviewService.Insert(reviewDTO.ToReview(id)));
        }

        [HttpPost]
        public async Task<ActionResult<Restaurant>> Insert([FromBody] AddRestaurantDTO objDTO)
        {
            var obj = await _service.Insert(objDTO.ToRestaurant());
            return CreatedAtAction(nameof(GetById), new { Id = obj.Id }, obj);
        }
    }
}