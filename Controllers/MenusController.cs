using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using FoodAPI.Services;
using FoodAPI.Models;
using FoodAPI.Models.Dtos;

namespace FoodAPI.Controllers
{
    [ApiController]
    [Route("api/v1/menus")]
    public class MenusController : ControllerBase
    {
        private readonly IMenuService _service;

        public MenusController(IMenuService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Menu>>> GetAll() 
        {
            return Ok(await _service.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Menu>> GetById(string id) 
        {
            return Ok(await _service.GetById(id));
        }

        [HttpPost]
        public async Task<ActionResult<Menu>> Insert([FromBody] AddMenuDTO objDTO)
        {
            var obj = await _service.Insert(objDTO.ToMenu());
            return CreatedAtAction(nameof(GetById), new { Id = obj.Id }, obj);
        }

        [HttpPost("insert-all")]
        public async Task<ActionResult<Menu>> InsertAll([FromBody] IEnumerable<AddMenuDTO> objsDTO)
        {
            var objs = await _service.InsertAll(objsDTO.Select(m => m.ToMenu()));
            return Created("", objs);
        }
    }
}