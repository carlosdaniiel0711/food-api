using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using FoodAPI.Services;
using FoodAPI.Models;
using FoodAPI.Models.Dtos;

namespace FoodAPI.Controllers
{
    [ApiController]
    [Route("api/v1/reviews")]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _service;

        public ReviewsController(IReviewService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Review>>> GetAll() 
        {
            return Ok(await _service.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Review>> GetById(string id) 
        {
            return Ok(await _service.GetById(id));
        }

        [HttpPut]
        public async Task<ActionResult<Review>> Update([FromBody] Review obj)
        {
            return Ok(await _service.Update(obj));
        }
    }
}