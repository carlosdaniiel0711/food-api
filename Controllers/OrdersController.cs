using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

using FoodAPI.Services;
using FoodAPI.Models;
using FoodAPI.Models.Dtos;

namespace FoodAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _service;
        private readonly AuthService _authService;

        public OrdersController(IOrderService service, AuthService authService)
        {
            _service = service;
            _authService = authService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetAll(){
            return Ok(await _service.GetByUserId(_authService.GetCurrentUserId()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetById(string id)
        {
            return Ok(await _service.GetById(id));
        }

        [HttpPost]
        public async Task<ActionResult<Order>> Insert([FromBody] AddOrderDTO orderDTO)
        {
            var order = await _service.Insert(orderDTO.ToOrder());
            return CreatedAtAction(nameof(GetById), new { Id = order.Id }, order);
        }

        [HttpPut("finish/{id}")]
        public async Task<ActionResult<Order>> FinishOrder(string id)
        {
            return Ok(await _service.Finish(id));
        }

        [HttpPut("rate/{id}")]
        public async Task<ActionResult<Order>> RateOrder(string id, [FromBody] RateOrderDTO objDTO)
        {
            objDTO.Validate();

            if (objDTO.Valid)
                return Ok(await _service.Rate(id, objDTO.Rating));
            else
                return BadRequest(objDTO.Notifications);
        }
    }
}