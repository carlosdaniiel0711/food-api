using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using FoodAPI.Services;
using FoodAPI.Models;
using FoodAPI.Models.Dtos;

namespace FoodAPI.Controllers
{
    [ApiController]
    [Route("api/v1/auth")]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _service;

        public AuthController(AuthService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<ActionResult<SuccessfulAuthResponseDTO>> Login([FromBody] AuthDTO auth)
        {
            return Ok(await _service.Login(auth));
        }
    }
}