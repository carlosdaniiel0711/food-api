using System;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

using FoodAPI.Models;
using FoodAPI.Services.Exceptions;

namespace FoodAPI.Middlewares
{
    public class GlobalErrorHandlerMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (SystemException e)
            {
                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, SystemException ex)
        {
            if (!context.Response.HasStarted)
            {
                ErrorResponse errorResponse;
                string responseAsJson;
                int statusCode;

                if (ex is ObjectNotFoundException)
                    statusCode = 404;
                else if (ex is Services.Exceptions.InvalidOperationException)
                    statusCode = 400;
                else
                    statusCode = 500;

                context.Response.StatusCode = statusCode;
                context.Response.ContentType = "application/json";

                errorResponse = new ErrorResponse(statusCode, ex.Message);
                responseAsJson = JsonConvert.SerializeObject(errorResponse);

                await context.Response.WriteAsync(responseAsJson);
            }
        }
    }
}