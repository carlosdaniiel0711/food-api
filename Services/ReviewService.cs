using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using FoodAPI.Context;
using FoodAPI.Models;
using FoodAPI.Services.Exceptions;
using FoodAPI.Utils;

namespace FoodAPI.Services
{
    public class ReviewService : IReviewService
    {
        private readonly DataContext _context;
        private readonly AuthService _authService;

        public ReviewService(DataContext context, AuthService authService)
        {
            _context = context;
            _authService = authService;
        }

        public async Task<IEnumerable<Review>> GetAll()
        {
            return await _context.Reviews
                .Include(r => r.User)
            .OrderByDescending(r => r.Date).ToListAsync();
        }

        public async Task<Review> GetById(string id)
        {
            var obj = await _context.Reviews
                .Include(r => r.User)
            .FirstOrDefaultAsync(r => r.Id.Equals(id));

            if (obj != null)
                return obj;
            else
                throw new ObjectNotFoundException("A review informada não foi encontrada");
        }

        public async Task<IEnumerable<Review>> GetByRestaurantId(string restaurantId)
        {
            return await _context.Reviews
                .Include(r => r.User)
                .Where(r => r.RestaurantId.Equals(restaurantId)).OrderByDescending(r => r.Date)
            .ToListAsync();
        }

        public async Task<Review> Insert(Review obj)
        {
            var user = await _authService.GetUserById(_authService.GetCurrentUserId());

            obj.Id = StringUtils.GetNewUUID();
            obj.Date = DateTime.Now;
            obj.Name = user.Name.Split(' ')[0];
            obj.User = user;

            await _context.Reviews.AddAsync(obj);
            await _context.SaveChangesAsync();

            return obj;
        }

        public async Task<Review> Update(Review obj)
        {
            _context.Entry<Review>(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return obj;
        }

        public async Task<IEnumerable<Review>> InsertAll(IEnumerable<Review> objs)
        {
            HashSet<Review> createdReviews = new HashSet<Review>();

            foreach(var obj in objs)
            {
                createdReviews.Add(await Insert(obj));
            }
            
            return createdReviews;
        }
    }
}