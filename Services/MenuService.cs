using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using FoodAPI.Context;
using FoodAPI.Models;
using FoodAPI.Services.Exceptions;
using FoodAPI.Utils;

namespace FoodAPI.Services
{
    public class MenuService : IMenuService
    {
        private readonly DataContext _context;

        public MenuService(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Menu>> GetAll()
        {
            return await _context.Menus.ToListAsync();
        }

        public async Task<Menu> GetById(string id)
        {
            var obj = await _context.Menus.FindAsync(id);

            if (obj != null)
                return obj;
            else
                throw new ObjectNotFoundException("O menu informado não foi encontrado");
        }

        public async Task<IEnumerable<Menu>> GetByRestaurantId(string restaurantId)
        {
            return await _context.Menus.Where(m => m.RestaurantId.Equals(restaurantId)).ToListAsync();
        }

        public async Task<Menu> Insert(Menu obj)
        {
            obj.Id = StringUtils.GetNewUUID();

            await _context.Menus.AddAsync(obj);
            await _context.SaveChangesAsync();

            return obj;
        }

        public async Task<IEnumerable<Menu>> InsertAll(IEnumerable<Menu> objs)
        {
            HashSet<Menu> createdMenus = new HashSet<Menu>();

            foreach(var obj in objs)
            {
                createdMenus.Add(await Insert(obj));
            }
            
            return createdMenus;
        }
    }
}