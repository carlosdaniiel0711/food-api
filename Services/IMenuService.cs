using System.Threading.Tasks;
using System.Collections.Generic;

using FoodAPI.Models;

namespace FoodAPI.Services
{
    public interface IMenuService
    {
        Task<IEnumerable<Menu>> GetAll();
        Task<Menu> GetById(string id);
        Task<IEnumerable<Menu>> GetByRestaurantId(string restaurantId);
        Task<Menu> Insert(Menu obj);
        Task<IEnumerable<Menu>> InsertAll(IEnumerable<Menu> objs);
    }
}