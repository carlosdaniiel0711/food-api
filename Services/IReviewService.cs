using System.Threading.Tasks;
using System.Collections.Generic;

using FoodAPI.Models;

namespace FoodAPI.Services
{
    public interface IReviewService
    {
        Task<IEnumerable<Review>> GetAll();
        Task<Review> GetById(string id);
        Task<IEnumerable<Review>> GetByRestaurantId(string reviewId);
        Task<Review> Insert(Review obj);
        Task<Review> Update(Review obj);
        Task<IEnumerable<Review>> InsertAll(IEnumerable<Review> objs);
    }
}