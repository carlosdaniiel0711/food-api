using System;

namespace FoodAPI.Services.Exceptions
{
    public class InvalidOperationException : SystemException
    {
        public InvalidOperationException(string message) : base(message)
        {
            
        }
    }
}