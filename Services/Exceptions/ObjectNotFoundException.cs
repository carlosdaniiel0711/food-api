using System;

namespace FoodAPI.Services.Exceptions
{
    public class ObjectNotFoundException : SystemException
    {
        public ObjectNotFoundException(string message) : base(message)
        {
            
        }
    }
}