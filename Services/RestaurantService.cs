using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using FoodAPI.Context;
using FoodAPI.Models;
using FoodAPI.Services.Exceptions;
using FoodAPI.Utils;

namespace FoodAPI.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly DataContext _context;

        public RestaurantService(DataContext context, IMenuService menuService)
        {
            _context = context;
        }

        public async Task<IEnumerable<Restaurant>> GetAll()
        {
            return await _context.Restaurants.ToListAsync();
        }

        public async Task<Restaurant> GetById(string id)
        {
            var obj = await _context.Restaurants
                .Include(r => r.Menus)
            .FirstOrDefaultAsync(r => r.Id.Equals(id));

            if (obj != null)
                return obj;
            else
                throw new ObjectNotFoundException("O restaurante informado não foi encontrado");
        }

        public async Task<Restaurant> Insert(Restaurant obj)
        {
            obj.Id = StringUtils.GetNewUUID();

            await _context.Restaurants.AddAsync(obj);
            await _context.SaveChangesAsync();

            return obj;
        }
    }
}