using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using FoodAPI.Services.Exceptions;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;

using FoodAPI.Models;
using FoodAPI.Models.Dtos;
using FoodAPI.Context;

namespace FoodAPI.Services
{
    public class AuthService
    {
        private readonly DataContext _context;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContext;

        public AuthService(DataContext context, IConfiguration configuration, IHttpContextAccessor httpContext)
        {
            _context = context;
            _configuration = configuration;
            _httpContext = httpContext;
        }

        public async Task<User> GetUserById(string userId)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
        }

        public async Task<SuccessfulAuthResponseDTO> Login(AuthDTO auth)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => (u.Login.Equals(auth.LoginOrEmail) || u.Email.Equals(auth.LoginOrEmail))
                    && u.Password.Equals(auth.Password) && u.Enabled);

            if (user != null)
            {
                user.LastLogin = DateTime.Now;

                await _context.SaveChangesAsync();

                return new SuccessfulAuthResponseDTO
                {
                    UserData = user,
                    AccessToken = CreateToken(user)
                };
            }
            else
            {
                throw new ObjectNotFoundException("Usuário/e-mail ou senha incorretos");
            }
        }

        public string GetCurrentUserId()
        {
            return _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public string GetCurrentUserEmail()
        {
            return _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.Email);
        }

        private string CreateToken(User user)
        {
            string jwtSecret = _configuration.GetSection("AppSettings:JwtSecret").Value;
            SymmetricSecurityKey secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecret));
            SigningCredentials credentials = new SigningCredentials(secret, SecurityAlgorithms.HmacSha512Signature);

            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Email, user.Email)
            };

            SecurityTokenDescriptor tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = credentials,
                Expires = DateTime.Now.AddDays(1)
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescription);

            return tokenHandler.WriteToken(token);
        }
    }
}