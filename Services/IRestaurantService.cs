using System.Threading.Tasks;
using System.Collections.Generic;

using FoodAPI.Models;

namespace FoodAPI.Services
{
    public interface IRestaurantService
    {
        Task<IEnumerable<Restaurant>> GetAll();
        Task<Restaurant> GetById(string id);
        Task<Restaurant> Insert(Restaurant obj);
    }
}