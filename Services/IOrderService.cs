using System.Threading.Tasks;
using System.Collections.Generic;

using FoodAPI.Models;

namespace FoodAPI.Services
{
    public interface IOrderService
    {

        Task<IEnumerable<Order>> GetAll();
        Task<Order> GetById(string id);
        Task<IEnumerable<Order>> GetByUserId(string userId);
        Task<Order> Insert(Order order);
        Task<Order> Finish(string orderId);
        Task<Order> Rate(string orderId, double rating);
    }
}