using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using FoodAPI.Context;
using FoodAPI.Models;
using FoodAPI.Services.Exceptions;
using FoodAPI.Utils;

namespace FoodAPI.Services
{
    public class OrderService : IOrderService
    {
        private readonly DataContext _context;
        private readonly AuthService _authService;

        public OrderService(DataContext context, AuthService authService)
        {
            _context = context;
            _authService = authService;
        }

        public async Task<IEnumerable<Order>> GetAll()
        {
            return await _context.Orders
                .Include(order => order.Items)
                .ThenInclude(orderItem => orderItem.MenuItem)
            .ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetByUserId(string userId)
        {
            return await _context.Orders
                .Include(order => order.Items)
                .ThenInclude(ordemItem => ordemItem.MenuItem)
            .Where(order => order.UserId.Equals(userId)).OrderByDescending(order => order.CreatedAt)
            .ToListAsync();
        }

        public async Task<Order> GetById(string id)
        {
            var order = await _context.Orders
                .Include(order => order.Items)
                .ThenInclude(orderItem => orderItem.MenuItem)
            .FirstOrDefaultAsync(order => order.Id.Equals(id));

            if (order != null)
                return order;
            else
                throw new ObjectNotFoundException($"O pedido código {id} não foi encontrado");
        }

        public async Task<Order> Insert(Order order)
        {
            // Menu item
            order.Items.ToList().ForEach(item =>
            {
                item.Id = StringUtils.GetNewUUID();
                item.TotalValue = item.MenuItem.Price * item.Qty;
                item.MenuItem = null;
            });

            // Order
            order.Id = StringUtils.GetNewUUID();
            order.TotalValue = order.Items.Sum(item => item.TotalValue);
            order.CreatedAt = DateTime.Now;
            order.Status = "CREATED";
            order.UserId = _authService.GetCurrentUserId();

            await _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();

            return order;
        }

        public async Task<Order> Finish(string orderId)
        {
            var order = await GetById(orderId);

            if (!order.IsDelivered())
            {
                order.Status = "DELIVERED";
                order.CompletedAt = DateTime.Now;

                await _context.SaveChangesAsync();

                return order;
            }
            else
            {
                throw new Exceptions.InvalidOperationException("Este pedido já está entregue");
            }
        }

        public async Task<Order> Rate(string orderId, double rating)
        {
            var order = await GetById(orderId);

            if (order.IsDelivered())
            {
                if (!order.IsRated())
                {
                    order.Rating = rating;

                    await _context.SaveChangesAsync();

                    return order;
                }
                else
                {
                    throw new Exceptions.InvalidOperationException("Este pedido já foi avaliado anteriormente");    
                }
            }
            else
            {
                throw new Exceptions.InvalidOperationException("Não é possível avaliar um pedido que ainda não foi entregue");
            }
        }
    }
}