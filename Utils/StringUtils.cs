using System;

namespace FoodAPI.Utils
{
    public static class StringUtils
    {
        public static string GetNewUUID()
        {
            return Guid.NewGuid().ToString();
        }
    }
}