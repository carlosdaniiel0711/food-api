using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using FoodAPI.Models;

namespace FoodAPI.Context.Maps
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ZipCode)
                .HasColumnName("ZipCode")
                .HasMaxLength(9);
        }
    }
}