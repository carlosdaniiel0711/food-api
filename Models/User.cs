using System;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace FoodAPI.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; set; }
        
        [JsonIgnore]
        public string Password { get; set; } // Plain-text password (temporary)
        
        [JsonIgnore]
        public bool Enabled { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}