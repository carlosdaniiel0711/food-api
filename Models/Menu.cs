using System.Text.Json.Serialization;

namespace FoodAPI.Models
{
    public class Menu
    {
        public string Id { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }

        [JsonIgnore]
        public Restaurant Restaurant { get; set; }

        [JsonIgnore]
        public string RestaurantId { get; set; }
    }
}