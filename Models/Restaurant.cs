using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace FoodAPI.Models
{
    public class Restaurant
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string DeliveryTime { get; set; }
        public float Rating { get; set; }
        public string Logo { get; set; }
        public string About { get; set; }
        public string Hours { get; set; }

        [JsonIgnore]
        public virtual ICollection<Menu> Menus { get; set; }

        [JsonIgnore]
        public virtual ICollection<Review> Reviews { get; set; }
    }
}