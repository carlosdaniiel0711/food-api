using System;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace FoodAPI.Models
{
    public class Order 
    {
        public string Id { get; set; }
        public string Customer { get; set; }
        public string Address { get; set; }
        public string Complement { get; set; }
        public string PaymentOption { get; set; }
        public double TotalValue { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? CompletedAt { get; set; }
        public double? Rating { get; set; }
        public string Status { get; set; }
        public virtual ICollection<OrderItem> Items { get; set; }
        
        [JsonIgnore]
        public User User { get; set; }

        [JsonIgnore]
        public string UserId { get; set; }

        public bool IsDelivered()
        {
            return Status.Equals("DELIVERED");
        }

        public bool IsRated()
        {
            return Rating.HasValue;
        }
    }
}