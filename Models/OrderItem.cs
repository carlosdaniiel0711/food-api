using System.Text.Json.Serialization;

namespace FoodAPI.Models
{
    public class OrderItem
    {
        public string Id { get; set; }
        public int Qty { get; set; }
        public double TotalValue { get; set; }

        [JsonIgnore]
        public Order Order { get; set; }

        [JsonIgnore]
        public string OrderId { get; set; }

        public Menu MenuItem { get; set; }

        [JsonIgnore]
        public string MenuItemId { get; set; }
    }
}