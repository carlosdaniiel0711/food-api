using System;
using System.Text.Json.Serialization;

namespace FoodAPI.Models
{
    public class Review
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public float Rating { get; set; }
        public string Comments { get; set; }

        [JsonIgnore]
        public Restaurant Restaurant { get; set; }

        [JsonIgnore]
        public string RestaurantId { get; set; }

        public User User { get; set; }

        [JsonIgnore]
        public string UserId { get; set; }
    }
}