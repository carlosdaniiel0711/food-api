using Flunt.Notifications;
using Flunt.Validations;

namespace FoodAPI.Models.Dtos
{
    public class RateOrderDTO : Notifiable, IValidatable
    {
        public double Rating { get; set; }

        public void Validate()
        {
            AddNotifications(new Contract()
                .IsBetween(Rating, 0, 5, "Rating", "A nota da avaliação precisa estar entre 0 (zero) e 5 (cinco)")
            );
        }
    }
}