using System.Linq;
using System.Collections.Generic;

namespace FoodAPI.Models.Dtos
{
    public class AddOrderDTO
    {
        public string Customer { get; set; }
        public string Address { get; set; }
        public string Complement { get; set; }
        public string PaymentOption { get; set; }
        public List<AddOrderItemDTO> Items { get; set; }

        public Order ToOrder()
        {
            return new Order
            {
                Customer = Customer,
                Address = Address,
                Complement = Complement,
                PaymentOption = PaymentOption,
                Items = Items.Select(item => item.ToOrdemItem()).ToList()
            };
        }
    }
}