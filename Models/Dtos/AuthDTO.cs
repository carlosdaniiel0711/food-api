namespace FoodAPI.Models.Dtos
{
    public class AuthDTO
    {
        public string LoginOrEmail { get; set; }
        public string Password { get; set; }
    }
}