using System;

namespace FoodAPI.Models.Dtos
{
    public class AddReviewDTO
    {
        public float Rating { get; set; }
        public string Comments { get; set; }

        public Review ToReview(string restaurantId)
        {
            return new Review
            {
                Rating = Rating,
                Comments = Comments,
                RestaurantId = restaurantId
            };
        }
    }
}