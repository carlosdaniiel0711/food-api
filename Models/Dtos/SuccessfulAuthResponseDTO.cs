namespace FoodAPI.Models.Dtos
{
    public class SuccessfulAuthResponseDTO
    {
        public User UserData { get; set; }
        public string AccessToken { get; set; }
    }
}