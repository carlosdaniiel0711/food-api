namespace FoodAPI.Models.Dtos
{
    public class AddOrderItemDTO
    {
        public int Qty { get; set; }
        public Menu MenuItem { get; set; }

        public OrderItem ToOrdemItem()
        {
            return new OrderItem
            {
                MenuItemId = MenuItem.Id,
                MenuItem = MenuItem,
                Qty = Qty
            };
        }
    }
}