namespace FoodAPI.Models.Dtos
{
    public class AddRestaurantDTO
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string DeliveryTime { get; set; }
        public float Rating { get; set; }
        public string Logo { get; set; }
        public string About { get; set; }
        public string Hours { get; set; }

        public Restaurant ToRestaurant()
        {
            return new Restaurant
            {
                Name = Name,
                Category = Category,
                DeliveryTime = DeliveryTime,
                Rating = Rating,
                Logo = Logo,
                About = About,
                Hours = Hours
            };
        }
    }
}