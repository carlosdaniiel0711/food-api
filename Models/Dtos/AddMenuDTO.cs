namespace FoodAPI.Models.Dtos
{
    public class AddMenuDTO
    {
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public string RestaurantId { get; set; }

        public Menu ToMenu()
        {
            return new Menu
            {
                Logo = Logo,
                Name = Name,
                Description = Description,
                Price = Price,
                RestaurantId = RestaurantId
            };
        }
    }
}