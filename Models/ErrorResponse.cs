using System;

namespace FoodAPI.Models
{
    public class ErrorResponse
    {
        public DateTime Date { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public ErrorResponse(int statusCode, string message)
        {
            Date = DateTime.Now;
            StatusCode = statusCode;
            Message = message;
        }
    }
}